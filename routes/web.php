<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.master');
// });

Route::get('/', function() {
    return redirect(route('login'));
});

Route::group(['middleware' => 'auth'], function() {

    Route::get('/home', 'HomeController@index')->name('home');
    // Middleware berdasarkan role
    Route::group(['middleware' => ['role:admin']], function() {
        Route::prefix('admin')->group(function() {
            Route::resource('/role', 'RoleController')->except(['create', 'show', 'edit', 'update']);
            Route::resource('/user', 'UserController')->except(['show']);
            Route::post('/users/permission', 'UserController@addPermission')->name('user.add_permission');
            Route::get('/users/role-permission', 'UserController@rolePermission')->name('user.roles_permission');
            Route::put('/users/permission/{role}', 'UserController@setRolePermission')->name('user.setRolePermission');
            Route::get('/users/role/{role}', 'UserController@roles')->name('user.role');
            Route::put('/users/role/{role}', 'UserController@setRole')->name('user.set_role');
        });
    });
    


    // Middleware berdasarkan permission
    Route::group(['middleware' => ['permission:create_product|edit_product|delete_product']], function() {
        Route::resource('category', 'CategoryController')->except(['create', 'show']);
        Route::resource('product', 'ProductController');
    });

    // Middleware berdasarkan role
    Route::group(['middleware' => ['role:kasir']], function() {
        Route::get('/transaksi', 'OrderController@addOrder')->name('order.transaksi');
    });

    Route::post('/cart', 'OrderController@addToCart');
    Route::get('/cart', 'OrderController@getCart');
    Route::delete('/cart/{id}', 'OrderController@removeCart');
});


Auth::routes();


