<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'code', 'name', 'description', 'stock', 'price', 'category_id', 'photo',
    ];

    // Jika tidak ingin pake $fillable bisa tuliskan ini
    // protected $guarded = [];

    public function category() {
        return $this->belongsTo(Category::class);
    }
}
