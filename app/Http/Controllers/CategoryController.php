<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $categories = Category::orderBy('created_at', 'DESC')->paginate(10);
        // dd($categories);
        return view('categories.index', compact('categories'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'description' => 'nullable|string'
        ]);

        try {
            $category = Category::firstOrCreate(
                ['name' => $request->name],
                ['description' => $request->description]
            );

            return redirect()->back()->with(['success' => 'Kategori: ' . $category->name . ' Ditambahkan.']);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    public function edit($id) {
        dd($id);
    }

    public function update(Request $request, $id) {
        // dd('oke');
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'description' => 'nullable|string'
        ]);

        try {
            $category = Category::findOrFail($id);
            $category->update([
                'name' => $request->name, 
                'description' => $request->description
            ]);

            return redirect()->back()->with(['success' => 'Kategori: ' . $category->name . ' Diperbarui']);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    public function destroy($id) {
        $category = Category::findOrFail($id);
        $category->delete();

        return redirect()->back()->with(['success' => 'Kategori: ' . $category->name . ' Telah Dihapus']);
    }
}
