<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('created_at', 'DESC')->paginate(10);

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::orderBy('name', 'ASC')->get();

        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'role' => 'required|string|exists:roles,name'
        ]);

        $user = User::firstOrCreate(
            [
                'email' => $request->email // mengutamakan email, hanya boleh satu email dalam table users
            ], 
            [
                'name' => $request->name,
                'password' => bcrypt($request->password),
                'status' => true
            ]
        );

        $user->assignRole($request->role); // menggunakan function assignRole() dari library Spatie
        
        return redirect(route('user.index'))->with(['success' => 'User <strong>' . $user->name . '</strong> Ditambahkan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        // return view('users.edit', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'email' => 'required|email|exists:users,email',
            'password' => 'nullable|min:6',
        ]);

        $user = User::findOrFail($id);

        // Jika password diisi berarti mau rubah password, 
        // Jika password tidak diisi berarti tidak rubah password, pake password yang lama
        $password = !empty($request->password) ? bcrypt($request->password) : $user->password;

        $user->update([
            'name' => $request->name,
            'password' => $password
        ]);

        return redirect(route('user.index'))->with(['success' => 'User <strong>' . $user->name . '</strong> Diperbaharui']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        
        return redirect()->back()->with(['success' => 'User <strong>' . $user->name . '</strong> Dihapus']);
    }

    public function rolePermission(Request $request) 
    {
        $role = $request->get('role'); // dapat dari role_permission.blade.php edit permission untuk role
        // dd($role);

        // Default, set dua buah variable dengan nilai null
        $permission = null;
        $hasPermission = null;

        // Mengambil data role
        $roles = Role::all()->pluck('name'); // return name saja fungsi dari pluck

        // Apabila parameter role terpenuhi
        if (!empty($role)) {
            // Select Role berdasarkan namenya, ini sejenis dengan find()
            $getRole = Role::findByName($role);

            // Query untuk mengambil permission yang telah dimiliki oleh role terkait
            $hasPermission = DB::table('role_has_permissions')
                ->select('permissions.name')
                ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                ->where('role_id', $getRole->id)->get()->pluck('name')->all();
            
            // dd($hasPermission);

            // Mengambil data permission
            $permissions = Permission::all()->pluck('name');
        }

        return view('users.role_permission', compact('roles', 'permissions', 'hasPermission'));
    }

    public function addPermission(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:permissions'
        ]);

        $permission = Permission::firstOrCreate([
            'name' => $request->name
        ]);

        return redirect()->back();
    }

    public function setRolePermission(Request $request, $role)
    {
        // Select role berdasarkan namanya
        $role = Role::findByName($role);

        // fungsi syncPermission akan menghapus semua permission yang dimiliki role tersebut,
        // kemudian di-assign kembali sehingga tidak terjadi duplicate data
        $role->syncPermissions($request->permission);

        return redirect()->back()->with(['success' => 'Permission sudah di set!']);
    }

    public function setRole(Request $request, $id)
    {
        $this->validate($request, [
            'role' => 'required'
        ]);

        $user = User::findOrFail($id);

        // fungsi syncRole akan menghapus semua role yang dimiliki,
        // kemudian di-assign kembali sehingga tidak terjadi duplicate data
        $user->syncRoles($request->role);

        return redirect()->back()->with(['success' => 'Role sudah di set!']);
    }

    public function roles(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all()->pluck('name');

        return view('users.roles', compact('user', 'roles'));
    }
}
