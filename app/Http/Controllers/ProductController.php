<?php

namespace App\Http\Controllers;

use File;
use Image;
use App\Product;
use App\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('category')->orderBy('created_at', 'DESC')->paginate();

        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name', 'ASC')->get();
        
        return view('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi data
        $this->validate($request, [
            'code' =>  'required|string|max:10|unique:products', 
            'name' => 'required|string|max:100', 
            'description' => 'nullable|string|max:100', 
            'stock' => 'required|integer', 
            'price' => 'required|integer', 
            'category_id' => 'required|exists:categories,id',
            'photo' => 'nullable|image|mimes:jpg,png,jpeg'
        ]);

        try {
            // default $photo = null
            $photo = null;
            // jika terdapat file(foto/gambar) yang dikirim
            if ($request->hasFile('photo')) {
                // maka menjalankan method saveFile()
                $photo = $this->saveFile($request->name, $request->file('photo'));
            }

            // Simpan data ke dalam table products
            $product = Product::create([
                'code' => $request->code, 
                'name' => $request->name, 
                'description' => $request->description, 
                'stock' => $request->stock, 
                'price' => $request->price, 
                'category_id' => $request->category_id, 
                'photo' => $photo
            ]);

            return redirect(route('product.index'))
                ->with(['success' => '<strong>' . $product->name . '</strong> Ditambahkan']);
        } catch (\Exception $e) {
            // Jika gagal, kembali ke halaman sebelumnya kemudian menampilkan error
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    private function saveFile($name, $photo) {
        // set nama file adalah gabungan antara nama produk dan time(). Ekstensi gambar tetap dipertahankan
        $image = str_slug($name, '_') . time() . '.' . $photo->getClientOriginalExtension();

        $path = public_path('uploads/product');

        // Cek jika uploads/product bukan direktori / folder
        if (!File::isDirectory($path)) {
            // maka folder tersebut dibuat
            File::makeDirectory($path, 0777, true, true);
        }

        // Simpan gambar yang diupload ke dalam folder uplads/product
        Image::make($photo)->save($path . '/' . $image);

        // Mengembalikan nama file yang ditampung dalam variable $image
        return $image;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = Category::orderBy('name', 'ASC')->get();

        return view('products.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validasi data
        $this->validate($request, [
            'code' =>  'required|string|max:10|exists:products,code', // mengecek apakah menggunakan code yang sama
            'name' => 'required|string|max:100', 
            'description' => 'nullable|string|max:100', 
            'stock' => 'required|integer', 
            'price' => 'required|integer', 
            'category_id' => 'required|exists:categories,id',
            'photo' => 'nullable|image|mimes:jpg,png,jpeg'
        ]);

        try {
            // query select berdasarkan id
            $product = Product::findOrFail($id);
            $photo = $product->photo;

            // cek jika ada file yang dikirim dari form
            if ($request->hasFile('photo')) {
                // cek jika photo tidak kosong maka file yang ada di folder uploads/product akan dihapus
                !empty($photo) ? File::delete(public_path('uploads/product/' . $product->photo)) : null;

                // uploading file photo yang baru menggunakan method saveFile() yang telah dibuat sebelumya
                $photo = $this->saveFile($request->name, $request->file('photo'));
            }

            // perbarui data di database
            $product->update([
                'name' => $request->name, 
                'description' => $request->description, 
                'stock' => $request->stock, 
                'price' => $request->price, 
                'category_id' => $request->category_id,
                'photo' => $photo
            ]);
            
            return redirect(route('product.index'))
                ->with(['success' => '<strong>' . $product->name . '</strong> Diperharui']);
        } catch (\Exception $e) {
            // Jika gagal, kembali ke halaman sebelumnya kemudian menampilkan error
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // query select berdasarkan id
        $product = Product::findOrFail($id);

        // mengecek, jika field photo tidak null / kosong
        if(!empty($product->photo)) {
            // file akan dihapus dari folder uploads/product
            File::delete(public_path('uploads/product/' . $product->photo));
        }

        // hapus data dari table
        $product->delete();

        return redirect()->back()->with(['success' => '<strong>' . $product->name . '</strong> Telah Dihapus!']);
    }
}
