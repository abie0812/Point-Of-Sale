<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function addOrder()
    {
        $products = Product::orderBy('created_at', 'DESC')->get();

        return view('orders.add', compact('products'));
    }

    public function getProduct($id)
    {
        $products = Product::findOrFail($id);

        return response()->json($products, 200);
    }

    public function addToCart(Request $request)
    {
        // Validasi data yang diterima
        // dari ajax request addToCart mengirimkan product_id dan qty
        $this->validate($request, [
            'product_id' => 'required|exists:products,id',
            'qty' => 'required|integer'
        ]);

        // Mengambil data product berdasarkan id
        $product = Product::findOrFail($request->product_id);
        // Mengambil cookie cart dengan $request->cookie('cart')
        $getCart = json_decode($request->cookie('cart'), true);

        // Jika ditanya ada
        if ($getCart) {
            // Jika key-nya exists berdasarkan product_id
            if (array_key_exists($request->product_id, $getCart)) {
                // Jumlahkan qty barangnya
                $getCart[$request->product_id]['qty'] += $request->qty;

                // dikirim kembali untuk disimpan ke cookie
                return response()->json($getCart, 200)
                    ->cookie('cart', json_encode($getCart), 120);
            }
        }

        // Jika cart kosong, maka tambahkan cart baru
        $getCart[$request->product_id] = [
            'code' => $product->code,
            'name' => $product->name,
            'price' => $product->price,
            'qty' => $request->qty
        ];

        // kirim responsenya kemudian simpan ke cookie
        return response()->json($getCart, 200)
            ->cookie('cart', json_encode($getCart), 120);
    }

    public function getCart()
    {
        // Mengambil cart dari cookie
        $cart = json_encode(request()->cookie('cart'), true);

        // Mengirimkan kembali dalam bentuk json untuk ditampilkan dengan vue.js
        return response()->json($cart, 200);
    }

    public function removeCart($id)
    {
        $cart = json_encode(request()->cookie('cart'), true);

        // Menghapus cart berdasarkan product_id
        unset($cart[$id]);

        // cart diperbaharui
        return response()->json($cart, 200)->cookie('cart', json_encode($cart), 120); // 120 adalah lama data tersimpan pada cookie dalam satuan menit
    }


}
