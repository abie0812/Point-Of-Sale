@extends('layouts.master')

@section('title', 'Manajemen Role')
@section('top-resource')

@endsection
@section('content')
<section class="content-header">
<h1>
    Dashboard
    <small>Control panel</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Manajemen Role</a></li>
    <li class="active">Role</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                @component('components.card')
                    @slot('header')
                        Tambah Role
                    @endslot

                    <form action="{{ route('role.store') }}" role="form" method="POST">
                        {{ csrf_field() }}
                        <div class="box-body">
                            @include('components.error')
                            @if(session('error'))
                                @component('components.alert', ['type' => 'danger'])
                                    {!! session('error') !!}
                                @endcomponent
                            @endif
                            <div class="form-group">
                                <label for="name">Role</label>
                                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" 
                                name="name" id="name" required
                                placeholder="Masukan role.."
                                value="{{ old('name') }}">
                                <p class="text-danger">{{ $errors->first('name') }}</p>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                @endcomponent
            </div>
            <div class="col-md-7">
                @component('components.card')
                    @slot('header')
                        Data Role
                    @endslot

                    <div class="box-body">
                        @if(session('success'))
                            @component('components.alert', ['type' => 'success'])
                                {!! session('success') !!}
                            @endcomponent
                        @endif
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Role</th>
                                    <th>Guard</th>
                                    <th>Created At</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $no = 1; @endphp
                                @forelse($roles as $row)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->guard_name }}</td>
                                        <td>{{ $row->created_at }}</td>
                                        <td>
                                            <form action="{{ route('role.destroy', $row->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>                                            
                                    </tr>
                                @empty
                                    <p colspan="5" class="text-center">Tidak ada data</p>
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Role</th>
                                    <th>Guard</th>
                                    <th>Created At</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="pull-right">
                            {!! $roles->links() !!}
                        </div>
                    </div>
                    <!-- /.box-body -->
                @endcomponent
            </div>
        </div>
    </div>
</section>
@endsection
@section('bottom-resource')
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection