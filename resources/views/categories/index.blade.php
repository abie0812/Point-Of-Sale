@extends('layouts.master')

@section('title', 'Manajemen Kategori')
@section('top-resource')

@endsection
@section('content')
<section class="content-header">
<h1>
    Dashboard
    <small>Control panel</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('user.index') }}"><i class="fa fa-dashboard"></i> Manajemen Kategori</a></li>
    <li class="active">Kategori</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                @component('components.card')
                    @slot('header')
                        Tambah Kategori
                    @endslot

                    <form action="{{ route('category.store') }}" role="form" method="POST">
                        {{ csrf_field() }}
                        <div class="box-body">
                            @include('components.error')
                            @if(session('error'))
                                @component('components.alert', ['type' => 'danger'])
                                    {!! session('error') !!}
                                @endcomponent
                            @endif
                            <div class="form-group">
                                <label for="name">Kategori</label>
                                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" 
                                name="name" id="name" required 
                                placeholder="Masukan nama kategori.."
                                value="{{ old('name') }}">
                                <p class="text-danger">{{ $errors->first('name') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="description">Deskripsi</label>
                                <textarea name="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" 
                                id="description" placeholder="Masukan deskripsi"
                                >{{ old('description') }}</textarea>
                                <p class="text-danger">{{ $errors->first('description') }}</p>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                @endcomponent
            </div>
            <div class="col-md-7">
                @component('components.card')
                    @slot('header')
                        Data Kategori
                    @endslot

                    <div class="box-body">
                        @if(session('success'))
                            @component('components.alert', ['type' => 'success'])
                                {!! session('success') !!}
                            @endcomponent
                        @endif
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kategori</th>
                                    <th>Deskripsi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $no = 1; @endphp
                                @forelse($categories as $row)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->description }}</td>
                                        <td>
                                            <form action="{{ route('category.destroy', $row->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <a href="{{ route('category.edit', $row->id) }}" class="btn btn-warning btn-sm" 
                                                    data-toggle="modal" data-target="#modal-default"><i class="fa fa-edit"></i></a>
                                                <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                            </form>

                                            @component('components.modal-edit')
                                                @slot('header')
                                                    Edit Kategori
                                                @endslot
                                                <form action="{{ route('category.update', $row->id) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('PUT') }}
                                                    <div class="box-body">
                                                        <div class="form-group">
                                                            <label for="name">Kategori</label>
                                                            <input type="text" class="form-control" name="name" id="name"                                 
                                                            value="{{ $row->name }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="description">Deskripsi</label>
                                                            <textarea name="description" class="form-control" 
                                                            id="description">{{ $row->description }}</textarea>
                                                        </div>
                                                    </div>
                                                    <!-- /.box-body -->
                                                    <div class="box-footer">
                                                        <button type="submit" class="btn btn-info">Update</button>
                                                    </div>
                                                </form>
                                            @endcomponent
                                            <!-- Large modal -->
                                            
                                    </tr>
                                @empty
                                    <p class="text-center">Tidak ada data</p>
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Kategori</th>
                                    <th>Deskripsi</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                @endcomponent
            </div>
        </div>
    </div>
</section>
@endsection
@section('bottom-resource')
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection