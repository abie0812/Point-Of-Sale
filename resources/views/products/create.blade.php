@extends('layouts.master')

@section('title', 'Tambah Produk')
@section('top-resource')

@endsection
@section('content')
<section class="content-header">
<h1>
    Dashboard
    <small>Control panel</small>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Manajemen Produk</a></li>
    <li class="active">Tambah Produk</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                @component('components.card')
                    @slot('header')
                        Tambah Produk
                    @endslot

                    <div class="box-body">
                        <form action="{{ route('product.store') }}" role="form" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                @include('components.error')
                                @if(session('error'))
                                    @component('components.alert', ['type' => 'danger'])
                                        {!! session('error') !!}
                                    @endcomponent
                                @endif
                                <div class="form-group">
                                    <label for="name">Kode Produk<sup>*</sup></label>
                                    <input type="text" name="code" id="code" required maxlength="10" 
                                    class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" 
                                    placeholder="Masukan kode produk.." value="{{ old('code') }}">
                                    <p class="text-danger">{{ $errors->first('code') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="name">Nama Produk<sup>*</sup></label>
                                    <input type="text" name="name" id="name" required 
                                    class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" 
                                    placeholder="Masukan nama produk.."
                                    value="{{ old('name') }}">
                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="description">Deskripsi</label>
                                    <textarea name="description" cols="5" rows="5" 
                                    class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" 
                                    id="description" placeholder="Masukan deskripsi.."
                                    >{{ old('description') }}</textarea>
                                    <p class="text-danger">{{ $errors->first('description') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="stock">Stok<sup>*</sup></label>
                                    <input type="number" name="stock" id="stock" min="0" placeholder="0" required 
                                    class="form-control {{ $errors->has('stock') ? 'is-invalid' : '' }}" 
                                    value="{{ old('stock') }}">
                                    <p class="text-danger">{{ $errors->first('stock') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="price">Harga<sup>*</sup></label>
                                    <input type="number" name="price" id="price" required 
                                    class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" 
                                    placeholder="Masukan harga.."
                                    value="{{ old('price') }}">
                                    <p class="text-danger">{{ $errors->first('price') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="category_id">Kategori<sup>*</sup></label>
                                    <select name="category_id" id="category_id" required
                                    class="form-control {{ $errors->has('category_id') ? 'is-invalid' : '' }}" >
                                        <option value="">Pilih</option>
                                        @foreach($categories as $row)
                                            <option value="{{ $row->id }}">{{ ucfirst($row->name) }}</option>
                                        @endforeach
                                    </select>
                                    <p class="text-danger">{{ $errors->first('category_id') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="price">Foto</label>
                                    <input type="file" name="photo" class="form-control">
                                    <p class="text-danger">{{ $errors->first('photo') }}</p>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                @endcomponent
            </div>
        </div>
    </div>
</section>
@endsection
@section('bottom-resource')
@endsection