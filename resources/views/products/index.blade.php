@extends('layouts.master')

@section('title', 'Tampilkan Product')
@section('top-resource')

@endsection
@section('content')
<section class="content-header">
<h1>
    Dashboard
    <small>Control panel</small>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Manajemen Produk</a></li>
    <li class="active">Data Produk</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                @component('components.card')
                    @slot('header')
                        <a href="{{ route('product.create') }}" class="btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> Tambah Produk</a>
                    @endslot

                    <div class="box-body">
                        @if(session('success'))
                            @component('components.alert', ['type' => 'success'])
                                {!! session('success') !!}
                            @endcomponent
                        @endif
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Gambar</th>
                                    <th>Nama Produk</th>
                                    <th>Stok</th>
                                    <th>Harga</th>
                                    <th>Kategori</th>
                                    <th>Last Update</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $no = 1; @endphp
                                @forelse($products as $row)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>
                                            @if(!empty($row->photo))
                                                <img src="{{ asset('uploads/product/' . $row->photo) }}" 
                                                alt="{{ $row->name }}" width="50px" height="50px;">
                                            @else
                                                <img src="http://via.placeholder.com/50x50" alt="{{ $row->name }}">
                                            @endif
                                        </td>
                                        <td>
                                            <sup class="label label-success">{{ $row->code }}</sup>
                                            <strong>{{ $row->name }}</strong>
                                        </td>
                                        <td>{{ $row->stock }}</td>
                                        <td>Rp {{ number_format($row->price) }}</td>
                                        <td>{{ $row->category->name }}</td>
                                        <td>{{ $row->updated_at }}</td>
                                        <td>
                                            <form action="{{ route('product.destroy', $row->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <a href="{{ route('product.edit', $row->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                                                <button class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>    
                                        </td>                                        
                                    </tr>
                                @empty
                                    <p colspan="7" class="text-center">Tidak Ada Data</p>
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Gambar</th>
                                    <th>Nama Produk</th>
                                    <th>Stok</th>
                                    <th>Harga</th>
                                    <th>Kategori</th>
                                    <th>Last Update</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="pull-right">
                            {!! $products->links() !!}
                        </div>
                    </div>
                    <!-- /.box-body -->
                @endcomponent
            </div>
        </div>
    </div>
</section>
@endsection
@section('bottom-resource')
@endsection