@extends('layouts.master')

@section('title', 'Transaksi')
@section('top-resource')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<section class="content-header">
<h1>
    Transaksi
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Transaksi</li>
</ol>
</section>

<!-- Main content -->
<section class="content" id="dw">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                @component('components.card')
                    @slot('header')
                        Transaksi
                    @endslot
                    <div class="row">
                        <div class="col-md-4">        
                            <!-- SUBMIT DIJALANKAN KETIKA TOMBOL DITEKAN -->
                            <form action="#" @submit.prevent="addToCart" method="POST">
                                <div class="form-group">
                                    <label for="">Produk</label>
                                    <select name="product_id" id="product_id"
                                    v-model="cart.product_id" class="form-control" required width="100%">
                                        <option value="">Pilih</option>
                                        @foreach($products as $product)
                                            <option value="{{ $product->id }}">{{ $product->code }} - {{ $product->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Qty</label>
                                    <input type="number" name="qty" v-model="cart.qty" id="qty" value="1" min="1" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-sm" :disabled="submitCart">
                                        <i class="fa fa-shopping-cart"> @{{ submitCart ? 'Loading...' : 'Ke Keranjang' }}</i>
                                    </button>
                                </div>
                            </form>
                        </div>
                        
                        <div class="col-md-8">
                            <div class="row">
                                <!-- MENAMPILKAN DETAIL PRODUCT -->
                                <div class="col-md-4">
                                    <h4>Detail Produk</h4>
                                    <div v-if="product.name">
                                        <table class="table table-striped">
                                            <tr>
                                                <th>Kode</th>
                                                <td>:</td>
                                                <td>@{{ product.code }}</td>
                                            </tr>
                                            <tr>
                                                <th width="3%">Produk</th>
                                                <td width="2%">:</td>
                                                <td>@{{ $product.name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Harga</th>
                                                <td>:</td>
                                                <td>@{{ product.price | currency }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <!-- MENAMPILKAN IMAGE DARI PRODUCT -->
                                <div class="col-md-4" v-if="product.photo">
                                    <img :src="'/uploads/photo/' + product.photo"
                                    height="150px" width="150px" :alt="product.name">
                                </div>
                            </div>
                        </div>
                    </div>
                @endcomponent
            </div>

            <div class="col-md-4">
                @component('components.card')
                    @slot('header')
                        Keranjang
                    @endslot
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Produk</th>
                                <th>Harga</th>
                                <th>Qty</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- MENGGUNAKAN LOOPING VUE.JS -->
                            <tr v-for="(row, index) in shoppingCart">
                                <td>@{{ row.name }} (@{{ row.code }})</td>
                                <td>@{{ row.price | currency }}</td>
                                <td>@{{ row.qty }}</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                @endcomponent
            </div>
        </div>
    </div>
</section>
@endsection
@section('bottom-resource')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js"></script>
    <script src="/js/transaksi.js"></script>
@endsection