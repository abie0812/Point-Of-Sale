<!DOCTYPE html>
<html>
<head>
  <title>Point Of Sale | @yield('title')</title>
  @include('layouts.module.cssplug')
  @yield('top-resource')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  @include('layouts.module.navbar')
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    @include('layouts.module.sidebar')
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @yield('content')

    {{-- <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section> --}}

    <!-- Main content -->
    {{-- <section class="content">
      
    </section> --}}
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('layouts.module.footer')

  <!-- Control Sidebar -->
  @include('layouts.module.control-sidebar')
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

@include('layouts.module.jsplug')
@yield('bottom-resource')
</body>
</html>
