  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ asset('template/adminlte/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Alexander Pierce</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li class="active">
        <a href="{{ route('home') }}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      @if(auth()->user()->can('create_product') || auth()->user()->can('edit_product') || auth()->user()->can('delete_product'))
      <li class="treeview">
        <a href="#">
          <i class="fa fa-pie-chart"></i>
          <span>Manajemen Produk</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('product.index') }}"><i class="fa fa-circle-o"></i> Produk</a></li>
          <li><a href="{{ route('category.index') }}"><i class="fa fa-circle-o"></i> Category</a></li>
        </ul>
      </li>
      @endif
      @role('admin')
      <li class="treeview">
        <a href="#">
          <i class="fa fa-users"></i>
          <span>Manajemen User</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('role.index') }}"><i class="fa fa-circle-o"></i> Role</a></li>
          <li><a href="{{ route('user.index') }}"><i class="fa fa-circle-o"></i> Users</a></li>
          <li><a href="{{ route('user.roles_permission') }}"><i class="fa fa-circle-o"></i> Role Permission</a></li>
        </ul>
      </li>
      @endrole
      @role('kasir')
      <li>
        <a href="{{ route('order.transaksi') }}">
          <i class="fa fa-shopping-cart"></i> Transaksi
        </a>
      </li>
      @endrole
      <li>
        <a href="{{ route('logout') }}"
        onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
          <i class="fa fa-sign-out"></i> <span>{{ __('Logout') }}</span>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none">
          {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->