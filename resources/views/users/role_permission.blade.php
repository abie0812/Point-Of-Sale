@extends('layouts.master')

@section('title', 'Role Permission')
@section('top-resource')
    <style type="text/css">
        .tab-pane{
            height:150px;
            overflow-y:scroll;
        }
    </style>
@endsection
@section('content')
<section class="content-header">
<h1>
    Permission
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('user.index') }}"><i class="fa fa-dashboard"></i> Manajemen User</a></li>
    <li class="active">Permission</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                @component('components.card')
                    @slot('header')
                        Tambah Permission
                    @endslot

                    <form action="{{ route('user.add_permission') }}" role="form" method="POST">
                        {{ csrf_field() }}
                        <div class="box-body">
                            @include('components.error')
                            @if(session('error'))
                                @component('components.alert', ['type' => 'danger'])
                                    {!! session('error') !!}
                                @endcomponent
                            @endif
                            <div class="form-group">
                                <label for="name">Permission</label>
                                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" 
                                name="name" id="name" required
                                placeholder="Masukan permission.."
                                value="{{ old('name') }}">
                                <p class="text-danger">{{ $errors->first('name') }}</p>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                @endcomponent
            </div>
            <div class="col-md-7">
                @component('components.card')
                    @slot('header')
                        Set Permission To Role
                    @endslot
                    @if(session('error'))
                        @component('components.alert', ['type' => 'success'])
                            {!! session('error') !!}
                        @endcomponent
                    @endif
                    {{-- Jika $permission bernilai kosong --}}
                    <form action="{{ route('user.roles_permission') }}" role="form" method="GET">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="role">Role</label>
                                <select name="role" id="role"
                                class="form-control {{ $errors->has('role') ? 'is-invalid' : '' }}" >
                                    @foreach($roles as $value)
                                        <option value="{{ $value }}" {{ request()->get('role') == $value ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <p class="text-danger">{{ $errors->first('role') }}</p>
                                <span class="input-group-btn">
                                    <button class="btn btn-danger">Check!</button>
                                </span>
                            </div>
                        </div>
                        <!-- /.box-body -->                        
                    </form>

                    {{-- Jika $permission bernilai tidak kosong --}}
                    @if(!empty($permissions))
                        <form action="{{ route('user.setRolePermission', request()->get('role')) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-group">
                                <div style="padding:10px;">
                                    <div class="nav nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1" data-toggle="tab">Permissions</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                @php $no = 1; @endphp
                                                @foreach($permissions as $key => $row)
                                                    <input type="checkbox" name="permission[]" 
                                                    class="minimal-red" 
                                                    value="{{ $row }}"
                                                    {{-- CEK, JIKA PERMISSION TERSEBUT SUDAH DI SET, MAKA CHECKED --}}
                                                    {{ in_array($row, $hasPermission) ? 'checked' : '' }}
                                                    > {{ $row }} <br>

                                                    @if($no++%4 == 0)
                                                        <br>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-send"></i> Set Permission</button>
                            </div>
                        </form>
                    @endif
                @endcomponent
            </div>
        </div>
    </div>
</section>
@endsection
@section('bottom-resource')
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection