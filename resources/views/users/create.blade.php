@extends('layouts.master')

@section('title', 'Tambah User')
@section('top-resource')

@endsection
@section('content')
<section class="content-header">
<h1>
    Tambah User
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Manajemen User</a></li>
    <li><a href="{{ route('user.index') }}">User</a></li>
    <li class="active">Tambah User</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                @component('components.card')
                    @slot('header')
                        Tambah User
                    @endslot

                    <div class="box-body">
                        <form action="{{ route('user.store') }}" role="form" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                @include('components.error')
                                @if(session('error'))
                                    @component('components.alert', ['type' => 'danger'])
                                        {!! session('error') !!}
                                    @endcomponent
                                @endif
                                <div class="form-group">
                                    <label for="name">Nama<sup>*</sup></label>
                                    <input type="text" name="name" id="name" required
                                    class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" 
                                    placeholder="Masukan nama.." value="{{ old('name') }}">
                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email<sup>*</sup></label>
                                    <input type="email" name="email" id="email" required
                                    class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" 
                                    placeholder="Masukan email.." value="{{ old('email') }}">
                                    <p class="text-danger">{{ $errors->first('email') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password<sup>*</sup></label>
                                    <input type="password" name="password" id="password" required
                                    class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" 
                                    placeholder="Masukan password.." value="{{ old('password') }}">
                                    <p class="text-danger">{{ $errors->first('password') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="role">Role<sup>*</sup></label>
                                    <select name="role" id="role" required
                                    class="form-control {{ $errors->has('role') ? 'is-invalid' : '' }}" >
                                        <option value="">Pilih</option>
                                        @foreach($roles as $row)
                                            <option value="{{ $row->name }}">{{ ucfirst($row->name) }}</option>
                                        @endforeach
                                    </select>
                                    <p class="text-danger">{{ $errors->first('role') }}</p>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                @endcomponent
            </div>
        </div>
    </div>
</section>
@endsection
@section('bottom-resource')
@endsection