@extends('layouts.master')

@section('title', 'Tampilkan User')
@section('top-resource')

@endsection
@section('content')
<section class="content-header">
<h1>
    Dashboard
    <small>Control panel</small>
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Manajemen User</a></li>
    <li class="active">User</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                @component('components.card')
                    @slot('header')
                        <a href="{{ route('user.create') }}" class="btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> Tambah User</a>
                    @endslot

                    <div class="box-body">
                        @if(session('success'))
                            @component('components.alert', ['type' => 'success'])
                                {!! session('success') !!}
                            @endcomponent
                        @endif
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $no = 1; @endphp
                                @forelse($users as $row)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->email }}</td>
                                        <td>
                                            @foreach($row->getRoleNames() as $role)
                                                <label class="label label-info">{{ $role }}</label>
                                            @endforeach
                                        </td>
                                        <td>
                                            @if($row->status)
                                                <label class="label label-success">Aktif</label>
                                            @else
                                                <label class="label label-default">Suspend</label>
                                            @endif
                                        </td>
                                        <td>
                                            <form action="{{ route('user.destroy', $row->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <a href="{{ route('user.role', $row->id) }}" class="btn btn-info btn-sm"><i class="fa fa-user-secret"></i></a>
                                                <a href="{{ route('user.edit', $row->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                                                <button class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <p colspan="6" class="text-center">Tidak Ada Data</p>
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="pull-right">
                            {!! $users->links() !!}
                        </div>
                    </div>
                    <!-- /.box-body -->
                @endcomponent
            </div>
        </div>
    </div>
</section>
@endsection
@section('bottom-resource')
@endsection