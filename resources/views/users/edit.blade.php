@extends('layouts.master')

@section('title', 'Edit User')
@section('top-resource')

@endsection
@section('content')
<section class="content-header">
<h1>
    Edit User
</h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Manajemen User</a></li>
    <li><a href="{{ route('user.index') }}">User</a></li>
    <li class="active">Edit User</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                @component('components.card')
                    @slot('header')
                        Edit User
                    @endslot

                    <div class="box-body">
                        <form action="{{ route('user.update', $user->id) }}" role="form" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="box-body">
                                @include('components.error')
                                @if(session('error'))
                                    @component('components.alert', ['type' => 'danger'])
                                        {!! session('error') !!}
                                    @endcomponent
                                @endif
                                <div class="form-group">
                                    <label for="name">Nama<sup>*</sup></label>
                                    <input type="text" name="name" id="name"
                                    class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" 
                                    value="{{ $user->name }}">
                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" readonly
                                    class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" 
                                    value="{{ $user->email }}">
                                    {{-- <p class="text-danger">{{ $errors->first('email') }}</p> --}}
                                    <p class="text-warning">Email user tidak dapat diganti</p>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password"
                                    class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}">
                                    <p class="text-danger">{{ $errors->first('password') }}</p>
                                    <p class="text-warning">Biarkan kosong, jika tidak ingin mengganti password</p>
                                </div>
                                <div class="form-group">
                                    <label for="role">Role</label>
                                    @foreach($user->getRoleNames() as $role)
                                        <input class="form-control" type="text" readonly value="{{ $role }}">
                                    @endforeach
                                    
                                    <p class="text-warning">Role tidak dapat diganti</p>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                @endcomponent
            </div>
        </div>
    </div>
</section>
@endsection
@section('bottom-resource')
@endsection