@extends('layouts.master')

@section('title', 'Set Role')
@section('top-resource')

@endsection
@section('content')
<section class="content-header">
<h1>
    Set Role
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('user.index') }}"><i class="fa fa-dashboard"></i> Manajemen User</a></li>
    <li class="active">Set Role</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                @component('components.card')
                    @slot('header')
                        Tambah Kategori
                    @endslot

                    <form action="{{ route('user.set_role', $user->id) }}" role="form" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="box-body">
                            @include('components.error')
                            @if(session('success'))
                                @component('components.alert', ['type' => 'success'])
                                    {!! session('success') !!}
                                @endcomponent
                            @endif
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <td>:</td>
                                            <td>{{ $user->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>:</td>
                                            <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                                        </tr>
                                        <tr>
                                            <th>Role</th>
                                            <td>:</td>
                                            <td>
                                                @foreach($roles as $row)
                                                    <input type="radio" name="role"
                                                    {{ $user->hasRole($row) ? 'checked' : '' }}
                                                    value="{{ $row }}"> {{ $row }} <br>
                                                @endforeach
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                @endcomponent
            </div>
        </div>
    </div>
</section>
@endsection
@section('bottom-resource')
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection 