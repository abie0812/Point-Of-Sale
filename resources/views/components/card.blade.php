<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">{{ $header }}</h3>
  </div>
  {{ $slot }}
</div>