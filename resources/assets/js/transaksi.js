import Vue from "vue";
import axios from 'axios';

Vue.filter('currency', function(money) {
    return accounting.formatMoney(money, "Rp ", 2, ".", ".")
})

new Vue({
    el: "#dw",
    data: {
        product: {
            id: '',
            qty: '',
            price: '',
            name: '',
            photo: ''
        }
    },
    watch: {
        // apabila nilai dari product > id berubah maka
        'product.id': function () {
            // mengecek jika nilai dari product > id ada
            if (this.product.id) {
                // maka akan menjalankan methods getProduct()
                this.getProduct()
            }
        }
    },
    // Menggunakan library select2 ketika file ini diload
    mounted() {
        $('#product_id').select2({
            width: '100%'
        }).on('change', () => {
            // Apabila terjadi perubahan nilai yang dipilih maka nilai tersebut
            // akan disimpan di dalam var product > id
            this.product.id = $('#product_id').val();
        })
    },
    methods: {
        getProduct() {
            // Fetch ke server menggunakan axios dengan mengirimkan parameter id
            // degan url /api/product/{id}
            axios.get(`/api/product/${this.product.id}`)
            .then((response) => {
                // Assign data yang diterima dari server ke var product
                this.product = response.data
                console.log(this.product)
            })
        }
    }
})